class Shape(object):

    def __init__(self, name, length, width):
        self.name = name
        self.length = length
        self.width = width

    # Abstract
    def area(self):
        raise NotImplementedError


class Rectangle(Shape):

    def area(self):
        return self.length * self.width


class Triangle(Shape):

    def __init__(self, name, length, height):
        super().__init__(name, length, height)

    def area(self):
        return self.length * self.width / 2


class Square(Rectangle):

    def __init__(self, name, length):
        super().__init__(name, length, length)


s1 = Rectangle("Rectangle", 3, 4)
s2 = Square("Square", 5)
s3 = Triangle("Triangle", 4, 5)
print(s1.area())
print(s2.area())
print(s3.area())