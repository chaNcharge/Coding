#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

struct MaxHeap {
    int size;
    int* array;
};

struct MaxHeap* createMaxHeap() {
    struct MaxHeap* maxHeap = (struct MaxHeap*)malloc(sizeof(struct MaxHeap));
    maxHeap->size = 0;
    maxHeap->array = (int*)malloc(MAX_SIZE * sizeof(int));
    return maxHeap;
}

void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void maxHeapify(struct MaxHeap* maxHeap, int index) {
    int largest = index;
    int left = 2 * index + 1;
    int right = 2 * index + 2;

    if (left < maxHeap->size && maxHeap->array[left] > maxHeap->array[largest])
        largest = left;

    if (right < maxHeap->size && maxHeap->array[right] > maxHeap->array[largest])
        largest = right;

    if (largest != index) {
        swap(&maxHeap->array[index], &maxHeap->array[largest]);
        maxHeapify(maxHeap, largest);
    }
}

void insert(struct MaxHeap* maxHeap, int value) {
    if (maxHeap->size >= MAX_SIZE) {
        printf("Heap is full, cannot insert more elements.\n");
        return;
    }

    maxHeap->array[maxHeap->size] = value;
    int current = maxHeap->size;
    maxHeap->size++;

    while (current != 0 && maxHeap->array[current] > maxHeap->array[(current - 1) / 2]) {
        swap(&maxHeap->array[current], &maxHeap->array[(current - 1) / 2]);
        current = (current - 1) / 2;
    }
}

void printMaxHeap(struct MaxHeap* maxHeap) {
    printf("Max Heap: ");
    for (int i = 0; i < maxHeap->size; i++) {
        printf("%d ", maxHeap->array[i]);
    }
    printf("\n");
}

int main() {
    struct MaxHeap* maxHeap = createMaxHeap();

    insert(maxHeap, 10);
    insert(maxHeap, 20);
    insert(maxHeap, 15);
    insert(maxHeap, 30);
    insert(maxHeap, 40);

    printMaxHeap(maxHeap);

    free(maxHeap->array);
    free(maxHeap);

    return 0;
}