"""
A height balanced binary tree is a binary tree in which the height 
of the left subtree and right subtree of any node does not differ 
by more than 1 and both the left and right subtree are also height balanced.
"""

class Node:
    def __init__(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right
    
root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
root.left.left.left = Node(8)

unbalanced = Node(1)
unbalanced.left = Node(2)
unbalanced.right = Node(3)
unbalanced.right.right = Node(4)
unbalanced.right.left = Node(5)
unbalanced.right.right.right = Node(8)

def height(node: Node):
    if node is None:
        return 0
    return max(height(node.left), height(node.right)) + 1 # type: ignore

print(height(root))

def balanced(node: Node):
    if node is None:
        return True
    
    if abs(height(node.left) - height(node.right)) <= 1 and balanced( # type: ignore
        node.left) is True and balanced(node.right) is True: # type: ignore
        return True
    return False

print(balanced(root))
print(balanced(root.right))
print(balanced(unbalanced))
