#include <iostream>

/**
 * A height balanced binary tree is a binary tree in which the height
 * of the left subtree and right subtree of any node does not differ
 * by more than 1 and both the left and right subtree are also height balanced.
 */

using namespace std;

class Node
{
public:
    Node(int d);
    ~Node();
    int data;
    Node *left;
    Node *right;
};

Node::Node(int d)
{
    data = d;
    left = nullptr;
    right = nullptr;
}

Node::~Node()
{
    delete left;
    delete right;
}

int height(Node *node)
{
    if (node == nullptr)
    {
        return 0;
    }
    return max(height(node->left), height(node->right)) + 1;
}

bool balanced(Node *node)
{
    if (node == nullptr)
    {
        return true;
    }
    if ((abs(height(node->left) - height(node->right)) <= 1) &&
        balanced(node->left) && balanced(node->right))
    {
        return true;
    }
    return false;
}

int main(int argc, char const *argv[])
{
    Node *root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);
    root->left->left->left = new Node(8);

    cout << "Height: " << height(root) << endl;
    cout << "Is balanced: " << balanced(root) << endl;

    delete root;

    return 0;
}
