from sympy.solvers import solve
from sympy.plotting import plot
from sympy import *

x = Symbol('x')
result = solve(16*x+10 - 1386)
print(result)
p1 = Integral((1/3)*x**3, (x, 1, x))
plot(p1.doit())