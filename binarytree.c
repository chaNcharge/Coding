#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/**
 * A height balanced binary tree is a binary tree in which the height
 * of the left subtree and right subtree of any node does not differ
 * by more than 1 and both the left and right subtree are also height balanced.
 */

struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *newNode(int data)
{
    struct Node *node = (struct Node *)malloc(sizeof(struct Node));
    node->data = data;
    node->left = NULL;
    node->right = NULL;

    return node;
}

int max(int a, int b) { return (a >= b) ? a : b; }

int height(struct Node *node)
{
    if (node == NULL)
    {
        return 0;
    }
    return max(height(node->left), height(node->right)) + 1;
}

bool balanced(struct Node *node)
{
    if (node == NULL)
    {
        return true;
    }
    if ((abs(height(node->left) - height(node->right)) <= 1) &&
        balanced(node->left) && balanced(node->right))
    {
        return true;
    }
    return false;
}

void freeTree(struct Node *tree)
{
    if (tree == NULL)
    {
        return;
    }

    freeTree(tree->left);
    freeTree(tree->right);
    free(tree);
}

int main(int argc, char const *argv[])
{
    struct Node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->right->right = newNode(7);
    root->left->left->left = newNode(8);

    printf("Height: %d\n", height(root));
    printf("Tree balanced? %d\n", balanced(root));

    freeTree(root);

    return 0;
}
