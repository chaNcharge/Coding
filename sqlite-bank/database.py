import sqlite3


class Customer(object):
    """A customer of ABC Bank with a checking account. 
    Database must be named 'database.db'. 
    Customers have the following properties:

    Attributes in database:
        id: Unique integer representing the customer's id. 
            Automatically assigned on creation.
        name: A string representing the customer's name.
        balance: A float tracking the current balance of the customer's account.
    """

    def __init__(self):
        """Return a Customer object whose name is *name* and starting
        balance is *balance*."""
        self.conn = sqlite3.connect('accounts.db')
        self.c = self.conn.cursor()

    def open(self):
        name = input("Account holder name: ")
        self.c.execute(
            "INSERT INTO accounts(name, amount) VALUES ('{0}', 0)".format(name))
        self.conn.commit()
        print("New account named {0} opened.".format(name))

    def close(self):
        idnumber = input("Account ID to CLOSE: ")
        self.c.execute(
            "SELECT name FROM accounts WHERE id IS {0}".format(idnumber))
        name = self.c.fetchall()
        self.c.execute("DELETE FROM accounts WHERE id IS {0}".format(idnumber))
        self.conn.commit()
        print("Account {0} closed.".format(name))

    def query(self):
        searchquery = input("Search by name: ")
        self.c.execute(
            "SELECT * FROM accounts WHERE trim(name) LIKE '{0}'".format(searchquery))
        print(self.c.fetchall())

    def deposit(self):
        idnumber = input("Enter account ID to deposit to: ")
        amount = input("Amount to deposit: ")
        self.c.execute(
            "UPDATE accounts SET amount = amount + {0} WHERE id IS {1}".format(amount, idnumber)
        )
        self.conn.commit()
        self.c.execute(
            "SELECT amount FROM accounts WHERE id IS {0}".format(idnumber)
        )
        print(self.c.fetchall())

    def withdraw(self):
        idnumber = input("Enter account ID to withdraw from: ")
        amount = input("Amount to withdraw: ")
        self.c.execute(
            "UPDATE accounts SET amount = amount - {0} WHERE id IS {1}".format(amount, idnumber)
        )
        self.conn.commit()
        self.c.execute(
            "SELECT amount FROM accounts WHERE id IS {0}".format(idnumber)
        )
        print(self.c.fetchall())


running = True
while running == True:
    a = Customer()
    choice = input("What would you like to do? ")
    if choice == "open":
        a.open()
    elif choice == "close":
        a.close()
    elif choice == "search" or choice == "query":
        a.query()
    elif choice == "exit":
        a.conn.close()
        running = False
    elif choice == "deposit":
        a.deposit()
    elif choice == "withdraw":
        a.withdraw()
    else:
        print("Invalid option.")
